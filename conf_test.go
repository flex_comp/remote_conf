package conf

import (
	"gitlab.com/flex_comp/comp"
	"testing"
	"time"
)

func TestRemoteConf(t *testing.T) {
	t.Run("all", func(t *testing.T) {
		comp.Init(map[string]interface{}{
			"env":        "prod",
			"log.path":   "./log/test.log",
			"log.size":   50,
			"log.age":    1,
			"etcd_hosts": []string{"http://192.168.1.25:2379"},
		})

		chClose := make(chan bool, 1)
		go func() {
			<-time.After(time.Second * 10)
			v := &struct {
				FirstPositionPer int `json:"first_position_per"`
				RiskMul          *struct {
					Normal    int `json:"normal"`
					Adventure int `json:"adventure"`
					Radical   int `json:"radical"`
				} `json:"risk_mul"`
				Profit *struct {
					Ratio         float64 `json:"ratio"`
					RatioCallback float64 `json:"ratio_callback"`
				} `json:"profit"`
				Stop struct {
					Ratios []*struct {
						Ratio float64 `json:"ratio"`
						Mul   int     `json:"mul"`
					} `json:"ratios"`
					RatioCallback float64 `json:"ratio_callback"`
				} `json:"stop"`
			}{}

			err := Unmarshal("strategy.spot_1", v)
			if err != nil {
				t.Fatal(err)
			}

			chClose <- true
		}()

		comp.Start(chClose)
	})
}
