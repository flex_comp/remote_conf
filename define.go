package conf

import (
	"github.com/spf13/viper"
	"gitlab.com/flex_comp/log"
	"sync"
	"time"
)

type confItem struct {
	sync.RWMutex
	path       string
	viper      *viper.Viper
	done       chan bool
	modifyPath chan string
}

func newConfItem(path string) *confItem {
	c := &confItem{
		done:       make(chan bool, 1),
		modifyPath: make(chan string, 1),
	}

	c.load(path)
	go func() {
		timer := time.NewTicker(time.Second)
		defer func() {
			timer.Stop()
		}()

		for {
			select {
			case <-c.done:
				return
			case path := <-c.modifyPath:
				c.load(path)
			case <-timer.C:
				err := c.viper.WatchRemoteConfig()
				if err != nil {
					log.Error(err)
				}
			}
		}
	}()

	return c
}

func (c *confItem) close() {
	select {
	case c.done <- true:
	default:
	}
}

func (c *confItem) setPath(path string) {
	select {
	case c.modifyPath <- path:
	default:
	}
}

func (c *confItem) get(key string) interface{} {
	c.RLock()
	defer c.RUnlock()

	return c.viper.Get(key)
}

func (c *confItem) load(path string) {
	c.Lock()
	defer c.Unlock()

	c.path = path

	c.viper = viper.New()
	err := c.viper.AddRemoteProvider("etcd", etcdHosts[0], c.path)
	if err != nil {
		log.Error(err)
		return
	}

	c.viper.SetConfigType("json")
	err = c.viper.ReadRemoteConfig()
	if err != nil {
		log.Error(err)
		return
	}
}
