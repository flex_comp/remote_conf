package conf

import (
	"errors"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"strings"
	"sync"
	"time"
)

var (
	ins       *RemoteConf
	etcdHosts []string
)

func init() {
	ins = &RemoteConf{
		configs: make(map[string]*confItem),
		done:    make(chan bool, 1),
	}

	_ = comp.RegComp(ins)
}

type RemoteConf struct {
	configs map[string]*confItem
	mtx     sync.RWMutex
	done    chan bool
}

func (r *RemoteConf) Init(m map[string]interface{}, args ...interface{}) error {
	etcdHosts = util.ToStringSlice(m["etcd_hosts"])
	if len(etcdHosts) == 0 {
		return errors.New("need env: ETCD_HOSTS=host")
	}

	viperConfList := viper.New()
	err := viperConfList.AddRemoteProvider("etcd", etcdHosts[0], "/config/list.json")
	if err != nil {
		return err
	}

	viperConfList.SetConfigType("json")
	err = viperConfList.ReadRemoteConfig()
	if err != nil {
		return err
	}

	go func() {
		timer := time.NewTicker(time.Second)
		defer func() {
			timer.Stop()
		}()

		for {
			select {
			case <-r.done:
				return
			case <-timer.C:
				err = viperConfList.WatchRemoteConfig()
				if err != nil {
					log.Error(err)
					continue
				}

				r.matchList(viperConfList)
			}
		}
	}()

	r.matchList(viperConfList)
	return nil
}

func (r *RemoteConf) Start(...interface{}) error {
	return nil
}

func (r *RemoteConf) UnInit() {
	select {
	case r.done <- true:
	default:
	}
}

func (r *RemoteConf) Name() string {
	return "remote-conf"
}

func Get(name string, key string) interface{} {
	return ins.Get(name, key)
}

func (r *RemoteConf) Get(name string, key string) interface{} {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	conf, ok := r.configs[name]
	if !ok {
		return nil
	}

	return conf.get(key)
}

func Unmarshal(name string, ref interface{}) error {
	return ins.Unmarshal(name, ref)
}

func (r *RemoteConf) Unmarshal(name string, ref interface{}) error {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	conf, ok := r.configs[name]
	if !ok {
		return nil
	}

	conf.RLock()
	defer conf.RUnlock()

	return conf.viper.Unmarshal(ref, func(c *mapstructure.DecoderConfig) {
		c.TagName = "json"
	})
}

func Keys(name string) []string {
	return ins.Keys(name)
}

func (r *RemoteConf) Keys(name string) []string {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	conf, ok := r.configs[name]
	if !ok {
		return nil
	}

	conf.RLock()
	defer conf.RUnlock()

	return conf.viper.AllKeys()
}

func (r *RemoteConf) matchList(v *viper.Viper) {
	m := make(map[string]interface{})
	_ = v.Unmarshal(&m)

	newList := util.ToStringMap(m)
	tmp := make(map[string]*confItem)

	r.mtx.Lock()
	defer r.mtx.Unlock()

	r.parse("", newList, tmp)

	// viper 只会合并字段, 导致比较移除不会触发
	//for name, item := range r.configs {
	//	_, ok := newList[name]
	//	if ok {
	//		tmp[name] = item
	//		continue
	//	}
	//
	//
	//	//del
	//	item.close()
	//}

	r.configs = tmp
}

func (r *RemoteConf) parse(rootName string, src map[string]interface{}, dst map[string]*confItem) {
	for name, v := range src {
		var rn string
		if len(rootName) == 0 {
			rn = name
		} else {
			rn = strings.Join([]string{rootName, name}, ".")
		}

		switch v.(type) {
		case string:
			old, ok := r.configs[rn]
			if !ok {
				// add
				dst[rn] = newConfItem(v.(string))
				continue
			}

			dst[rn] = old
			if old.path != v.(string) {
				// modify
				old.setPath(v.(string))
				continue
			}
		case map[string]interface{}:
			r.parse(rn, v.(map[string]interface{}), dst)
		}
	}
}
